# Операционные системы и сети

Создайте Vagrant-проект, поместив туда ваш фреймворк/среду, с которой вы обычно работаете в повседневной практике.

## Задачи

1. Инициализируйте Vagrant-проект
1. Настройте систему с помощью Ansible Provisioning
1. Инициализируйте проект на вашем фреймворке, убедитесь что он работает
1. Выставьте наружу нужный порт и проверьте что запущенный сервер доступен из браузера
1. Поэкспериментируйте с Vagrant. Попробуйте пересоздать Vagrant-машину
